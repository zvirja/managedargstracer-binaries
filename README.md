ManagedArgsTracer Binaries repository
=====================================

### This repository was designed to host binaries with ManagedArgsTracer tool. This repository doesn't contain Issue Tracker or Wiki. ###

#### Refer to the [https://bitbucket.org/zvirja/managedargstracer](https://bitbucket.org/zvirja/managedargstracer) repository to get [Wiki](https://bitbucket.org/zvirja/managedargstracer/wiki/Home) and [Issue Tracker](https://bitbucket.org/zvirja/managedargstracer/issues?status=new&status=open). ####

#### Refer to the [https://bitbucket.org/zvirja/managedargstracer-binaries/downloads](https://bitbucket.org/zvirja/managedargstracer-binaries/downloads) page to download the latest version of tool. ####